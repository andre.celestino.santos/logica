import java.util.Arrays;
import java.util.List;

public class BirthdayCakeCandles {

	public static void main(String[] args) {
		List<Integer> candles = Arrays.asList(4,4,1,3);
		System.out.println(birthdayCakeCandles(candles));
	}
	
	public static int birthdayCakeCandles(List<Integer> candles) {
		// max verification
		int arraySize = candles.size();
		int numberMax = 0;
		for(int i = 0; i < arraySize; i++) {
			int maxCount = 0;
			for(int j = 0; j < arraySize; j++) {
				if(candles.get(i) >= candles.get(j)) {
					maxCount++;
				}
			}
			if(maxCount >= arraySize) {
				numberMax = candles.get(i);
				break;
			}
		}
		
		int countCandle = 0;
		// count candles max
		for(int i = 0; i < arraySize; i++) {
			if(candles.get(i) == numberMax)
				countCandle++;
		}
		
		return countCandle;
	}

}
