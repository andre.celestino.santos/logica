import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class DoSwing {
	static Object[] columnNames = {"name","age","country"};
	static Object[][] data = {
			                   {"andre",28,"brazil"},
			                   {"john",30,"united states"}
			                 };
	
	public static void main(String... args) {
		JFrame jf = new JFrame("my first program with java swing");
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jf.setLocationRelativeTo(null); // show frame in center screen
		jf.setUndecorated(true); // disable everyone buttons the frame
		jf.getContentPane()
		  .setLayout(new GridBagLayout());
		
		//------------
		GridBagConstraints c1 = new GridBagConstraints();
		c1.fill = GridBagConstraints.HORIZONTAL;
		
		JButton jb1 = new JButton("Inserir");
		
		c1.weightx = 0.5;
		c1.gridx = 0;
		c1.gridy = 1;
		c1.insets = new Insets(100, 10, 0, 10);
		
		jf.getContentPane()
		  .add(jb1, c1);
		
		JButton jb2 = new JButton("Limpar");
		
		c1.gridy = 1;
		c1.gridx = 1;
		
		jf.getContentPane()
		  .add(jb2, c1);
		
		//------------
		GridBagConstraints c2 = new GridBagConstraints();
		
		JTable jt = new JTable(new DefaultTableModel(data, columnNames));
		jt.setPreferredScrollableViewportSize(new Dimension(300, 70));
		
		c2.fill = GridBagConstraints.HORIZONTAL;
		c2.weightx = 0.5;
		c2.gridx = 0;
		c2.gridy = 0;
		c2.gridwidth = 2;

		JScrollPane sp = new JScrollPane(jt);
		
		jf.getContentPane()
		  .add(sp, c2);
		
		//------------
		jf.pack();
		jf.setVisible(true);
		
		jb2.addActionListener(e -> {
			while(jt.getRowCount() > 0) {
				System.out.println("removendo...");
				((DefaultTableModel)jt.getModel()).removeRow(0);
				System.out.println("removeu!");
			}
		});
	}

}