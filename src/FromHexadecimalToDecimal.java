
public class FromHexadecimalToDecimal {

	public static void main(String[] args) {
		System.out.println(from("1A82")); // decimal number -> 6786
	}
	
	// converter from hexadecimal to decimal number 
	static int from(String hexadecimal) {
		int number = 0;
		
		// converter from hexadecimal to char array 
		char[] charsArray = hexadecimal.toCharArray();
		int charArraySize = charsArray.length;
		
		// agregade decimal numbers
		int counts[] = new int[charArraySize];
		for(int i = 0; i < charArraySize; i++) {
			int multiplicateNumber = fromTextToNumber(charsArray[i]);
			int elevateNumber = elevate(charArraySize-(i+1));
			
			if(elevateNumber > 0) {
				counts[i] = multiplicateNumber * elevateNumber; // multiplication
			}else {
				counts[i] = multiplicateNumber;
			}
		}
		
		// calculate decimal number
		for(int i = 0; i < charArraySize; i++) {
			number += counts[i];
		}
		
		return number;
	}
	
	// converter letter to decimal number
	static int fromTextToNumber(char text) {
		String textString = String.valueOf(text);
		int number = 0;
		if (textString.codePointAt(0) > 64 && textString.codePointAt(0) < 71) { // letter between A and F
			String[] hexadecimalLetter = { "A", "B", "C", "D", "E", "F" };
			int[] numbers =              {  10,  11,  12,  13,  14,  15 };
			int hexadecimalLetterArraySize = hexadecimalLetter.length;

			// search letter for find the number
			for (int i = 0; i < hexadecimalLetterArraySize; i++) {
				if (textString.equals(hexadecimalLetter[i])) {
					number = numbers[i];
				}
			}
			return number;
		}
		return Integer.parseInt(textString);
	}
	
	// number elevate 16
	static int elevate(int the) {
		int number = 0;
		if(the == 1)
			number = 16;
		for(int i = 1; i < the; i++) {
			if(number == 0) {
				number = 16 * 16;
			}else {
				number *= 16;	
			}
		}
		return number;
	}

}
