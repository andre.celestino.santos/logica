
public class TimeConversion {
	
	public static void main(String... args) {
		System.out.println(timeConversion("12:01:50AM"));
	}
	
    /*
     * Complete the timeConversion function below.
     */
    static String timeConversion(String s) {
    	String time24hs="";
    	String hour = s.substring(0, 2);
    	String min = s.substring(3, 5);
    	String seconds = s.substring(6, 8);
    	
    	boolean isPm = s.substring(s.length()-2).equals("PM") ? true : false;
    	
    	int midday = 12;
    	if(isPm) {
    		if(Integer.parseInt(hour) != midday) {
    			hour = String.valueOf(Integer.parseInt(hour) + midday);
    		}
    	} else {
    		if(Integer.parseInt(hour) == midday) {
    			hour = "00";
    		}
    	}
    	    	
    	time24hs = hour + ":" + min + ":" + seconds; // new format
    	
    	return time24hs;
    }

}
