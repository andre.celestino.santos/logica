
public class AppleandOrange {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int[] apples = {2,3,-4};
		int[] oranges = {3, -2, -4};
		
		countApplesAndOranges(7, 10, 4, 12, apples, oranges);
	}
	
	 // Complete the countApplesAndOranges function below.
    static void countApplesAndOranges(int s, int t, int a, int b, int[] apples, int[] oranges) {
    	// variables count fruit falls in house the Sam's
    	int countApples = 0;
    	int countOranges = 0;
    	
    	// verification and increment apples
    	int applesArraySize = apples.length;
    	for(int i = 0; i < applesArraySize; i++) {
    		if((a + apples[i]) >= s && (a + apples[i]) <= t) {
    			countApples++;
    		}
    	}
    	
    	// verification and increment oranges
    	int orangesArraySize = oranges.length;
    	for(int i = 0; i < orangesArraySize; i++) {
    		if((b + oranges[i]) >= s && (b + oranges[i]) <= t) {
    			countOranges++;
    		}
    	}
    	
    	// print fist apple and segund oranges
    	System.out.println(countApples);
    	System.out.println(countOranges);
    }

}
