
public class FromDecimalToHexadecimal {

	public static void main(String[] args) {
		System.out.println(from(0));
		System.out.println(from(1));
		System.out.println(from(2));
		System.out.println(from(3));
		System.out.println(from(4));
		System.out.println(from(5));
		System.out.println(from(6));
		System.out.println(from(7));
		System.out.println(from(8));
		System.out.println(from(9));
		System.out.println(from(10));
		System.out.println(from(11));
		System.out.println(from(12));
		System.out.println(from(13));
		System.out.println(from(14));
		System.out.println(from(15));
		System.out.println(from(16));
		System.out.println(from(255));
		System.out.println(from(300));
		System.out.println(from(6786));
	}
	
	static String from(int number) {
		String hexadecimal = "";
		String[] hexadecimalBase  = {"A","B","C","D","E","F"};
		
		if(number > -1 && number < 10) { // from 0 to 9
			return hexadecimal + number;	
		} else if(number > 9 && number < 16) { // from 10 to 15
			for(int i = 10; i < 16; i++) {
				if(i == number)
					return hexadecimalBase[i-10];
			}	
		} 
		// from 16 to infinite
		int [] restNumbers = new int[0];
		
		// get divisions
		do {
			restNumbers = add(restNumbers, number % 16);
			number = number / 16;
			if(number == 1)
				restNumbers = add(restNumbers, number % 16);
		} while(number >= 15);
		
		// build hexadecimal
		int restNumbersArraySize = restNumbers.length;
		
		for (int i = restNumbersArraySize - 1; i >= 0; i--) {
			if (restNumbers[i] > -1 && restNumbers[i] < 10) { // from 0 to 9
				hexadecimal += restNumbers[i];
			} else {
				for (int j = 10; j < 16; j++) { // from 10 to 15
					if (j == restNumbers[i])
						hexadecimal += hexadecimalBase[j - 10];
				}
			}
		}
				
		return hexadecimal;
	}
	
	static int[] add(int[] array, int number) {
		int size = array.length; // size arry old 
		int arrayNewSize = size + 1; // size new array
		int[] arrayNew = new int[arrayNewSize]; // build new array
		
		// copy from old to new
		for(int i = 0; i < size; i++) { 
			arrayNew[i] = array[i];
		}
		
		arrayNew[arrayNewSize - 1] = number; // add new value last position
		
		return arrayNew;
	}

}
