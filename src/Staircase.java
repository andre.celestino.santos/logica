
public class Staircase {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int staircaseSize = 10;
		
		staircase(staircaseSize);
	}
	
	// Complete the staircase function below.
    static void staircase(int n) {
    	for(int i = 0; i < n; i++) { // iterations rows
    		int aux = (n -1) - i; // variable aux for start last position
    		for(int j = 0; j < n; j++) { // iterations columns
    			if(j >= aux) { // start position
    				System.out.print("#");
    			} else {
    				System.out.print(" "); // space    				
    			}
    			
    			if(j == (n - 1)) // line return - new row
    				System.out.println();
    		}
    	}
    }

}