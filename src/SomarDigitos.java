/*
	Sem utilizar recursos de string, ou seja, trabalhe apenas com tipos num�ricos.

	Somar d�gitos significa que dados um n�mero qualquer, exemplo, 2015, devemos somar seus d�gitos:

	2  +  0 +  1 +  5
    O resultado esperado para o n�mero acima seria 8. 
*/

public class SomarDigitos {

	public static void main(String... args) {
		int numero = 2015; // numero informado

		int soma = 0;
		
		while (numero != 0) {
	        soma   += numero % 10; // resto da divisao base decimal "10" 
	        numero  = numero / 10; // pega a parte inteira da divisao
	    }
		
		System.out.println(soma);
	}
	
}
