
public class NumberLineJumps {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		// x posicao
		// v saltos por pulo
		
		System.out.println(kangaroo(0, 3, 4, 2)); // YES - 1� comecou antes e pulou 3  - 2� comecou depois e pulou 2
		System.out.println(kangaroo(0, 2, 5, 3)); // NO -  1� comecou antes e pulou 2  - 2� comecou depois e pulou 3
		System.out.println(kangaroo(2, 1, 1, 2)); // YES - 1� comecou depois e pulou 1 - 2� comecou antes e pulou 2
	}
	
	 // Complete the kangaroo function below.
    static String kangaroo(int x1, int v1, int x2, int v2) {
    	if(v1 > v2 && (x2 - x1) % (v1 - v2) == 0)
    		return "YES";

    	return "NO";
    }


}
