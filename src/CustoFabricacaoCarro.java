
public class CustoFabricacaoCarro {
	
	public static void main(String... args) {
		int custoFabrica = 10000;
		double percentualDistribuidor = 0.28;
		double percentualImposto = 0.45;
		
		double custoFinal = custoFabrica + (custoFabrica * percentualDistribuidor) + (custoFabrica * percentualImposto);
		
		System.out.println("Custo final: " + custoFinal);
	}

}