/*
 Fa�a um programa para identificar se um n�mero � primo.

 Lembre-se que n�mero primo, � um n�mero natural, maior que 1, apenas divis�vel por si pr�prio e pela unidade.
 */
public class NumerosPrimos {
	
	public static void main(String... argss) {
		boolean ePrimo = false;
		double unidade = 1.0;
		
		double numero = 9.0; // 5 � primo
	    //double numero = 5.5; // 5.5 nao � primo
	    //double numero = 10; // 10 nao � primo
		//double numero = 73; // 73 � primo
		
		 /* divisao pela propria unidade "1.0"
		    convertendo para pegar apenas a parte inteira da divisao
		    assim conseguimos saber se a divisao foi de um numero inteiro 
		 */
		int divisaoUnidade = (int) (numero / unidade);
		
		if(divisaoUnidade == numero && (numero / numero == unidade)) { // verifica divisao por "1" e divisao por ele mesmo
			// verificar se o numero � divisivel por algum outro numero
			for(int i = 2; i < numero; i++) {
				double divisao = numero / i; // realizar a divisao do numero pelo contador.
				int divisaoInteiro = (int) divisao; // converte um double em inteiro.
				if(divisaoInteiro != divisao) {
					ePrimo = true;
				} else {
					i += numero; // incrementa contador para sair do loop
					//break; // � possivel tambem utilizar o comando break para sair do loop
				}
			}
		}
		
		if(ePrimo) {
			System.out.println("o numero " + numero + " � primo");
		}else {
			System.out.println("o numero " + numero + " nao � primo");
		}
		
	}

}