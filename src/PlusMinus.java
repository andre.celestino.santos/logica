import java.util.Locale;

public class PlusMinus {

	public static void main(String[] args) {
		int[] arr = {1,1,0,-1,-1};
		
		plusMinus(arr);
	}
	
	// Complete the plusMinus function below.
    static void plusMinus(int[] arr) {
    	int positive = 0;
    	int negative = 0;
    	int zero = 0;
    	int arraySize = arr.length;
    	
    	// calculate numbers positive, negative and zero
    	for(int i = 0; i < arraySize; i++) {
    		if(arr[i] > 0) {
    			positive++;
    		}else if(arr[i] < 0) {
    			negative++;
    		}else {
    			zero++;
    		}
    	}
    	
    	// print with 6 plates decimal and locale in english
    	System.out.println(String.format(Locale.ENGLISH,"%.6f", (double) positive / arraySize));
    	System.out.println(String.format(Locale.ENGLISH,"%.6f", (double) negative / arraySize));
    	System.out.println(String.format(Locale.ENGLISH,"%.6f", (double) zero / arraySize));
    }

}



