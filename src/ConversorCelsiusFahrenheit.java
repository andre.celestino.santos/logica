
public class ConversorCelsiusFahrenheit {

	public static void main(String... args) {
		//Celsius para Fahrenheit
		double grauCelsius = 24;
		
		System.out.println(grauCelsius + " grau Celsius equivale a " + String.format("%.2f", fahrenheit(grauCelsius)) + " Fahrenheit");
		
		//Fahrenheit para Celsius
		double grauFahrenheit = 75.2;
		
		System.out.println(grauFahrenheit +" fahrenheit equivale a " + String.format("%.2f", celsius(grauFahrenheit)) + " Celsius");
	}
	
	private static double fahrenheit(double grauCelsius) {
		return (grauCelsius * 1.80) + 32.0; //calculo para conversao em fahrenheit
	}
	
	private static double celsius(double grauFahrenheit) {
		return ((grauFahrenheit - 32.0) / 1.80); //calculo para conversao em celsius
	}

}