
public class MinMaxSum {

	public static void main(String[] args) {
		//int[] arr = {1,2,3,4,5};
		//int[] arr = {1,3,5,7,9};
		int[] arr = {7,69,2,221,8974};
		//int[] arr = {-1,2,3,4,5};
		
		miniMaxSum(arr);
	}
	
	 // Complete the miniMaxSum function below.
    static void miniMaxSum(int[] arr) {
    	int arraySize = arr.length; // get array size
    	long[] sums = new long[arraySize];
    	for(int i = 0; i < arraySize ; i++) {
    		long sum = 0; // declare sum variable 64bits Long.BYTES*8 = 64
        	for(int j = 0; j < arraySize; j++ ) {
        		if(j != i)
        			sum += arr[j];
        	}
        	sums[i] = sum;
    	}
    	
    	long min = 0;
    	long max = 0;
        	
    	// min
    	for(int i = 0; i < arraySize; i++) {
    		int countMin = 0;
    		int countEq = 0;
    		for(int j = 0 ; j < arraySize; j++) {
    			if(sums[i] < sums[j]) {
    				countMin++;
    			}
    			if(sums[i] == sums[j]) {
    				countEq++;
    			}
    		}
    		if(countMin == 4 || countEq > 1)
    			min = sums[i];
    	}
    	
    	// max
    	for(int i = 0; i < arraySize; i++) {
    		int countMax = 0;
    		int countEq = 0;
    		for(int j = 0 ; j < arraySize; j++) {
    			if(sums[i] > sums[j]) {
    				countMax++;
    			} 
    			if(sums[i] == sums[j]) {
    				countEq++;
    			}
       		}
    		if(countMax == 4 || countEq > 1)
    			max = sums[i];
      	}
    	
    	System.out.println(min + " " + max);
    }
	
}
