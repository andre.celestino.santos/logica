import java.io.IOException;

public class OldTheGame {
	
	public static void main(String[] args) throws IOException {
		String[][] date = new String[3][3];
        date[0][0] = "X";
		date[0][1] = "X";
		date[0][2] = "X";
		
		print(date);
		
		if(isWin(date))
			System.out.println("you win!");
	}
	
	private static boolean isWin(String[][] date) {
		
		return false;
	}

	// print current game
	private static void print(String[][] date) {
		// first row
		System.out.print(date[0][0]   != null ?   " " + date[0][0] + " |" : "   |");
		System.out.print(date[0][1]   != null ?   " " + date[0][1] + " |" : "   |");
		System.out.println(date[0][2] != null ?   " " + date[0][2] + "  " : "");
		System.out.println("-----------");
		
		// second row
		System.out.print(date[1][0]   != null ?   " " + date[1][0] + " |" : "   |");
		System.out.print(date[1][1]   != null ?   " " + date[1][1] + " |" : "   |");
		System.out.println(date[1][2] != null ?   " " + date[1][2] + "  " : "");
		System.out.println("-----------");
		
		// third row
		System.out.print(date[2][0]   != null ?   " " + date[2][0] + " |" : "   |");
		System.out.print(date[2][1]   != null ?   " " + date[2][1] + " |" : "   |");
		System.out.println(date[2][2] != null ?   " " + date[2][2] + "  " : "");
	}

}