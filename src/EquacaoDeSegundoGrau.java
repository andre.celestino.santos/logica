/*
 http://devfuria.com.br/logica-de-programacao/equacao-2grau/ 
 
Exercício de lógica de programação
Utilizando funções, faça um programa que calcule as raízes da equação do 2 grau conforme a fórmula de Bhaskara.

Lembrando:

ax(2) + bx + c = 0

delta = (Δ = b2 - 4.a.c)
x1    = ( (-b + √Δ)/2a)
x2    = ( (-b - √Δ)/2a)
 */

public class EquacaoDeSegundoGrau {

	public static void main(String[] args) {
		int a, b, c;
		a = 1;
		b = 0;
		c = -16;
		
		int delta = delta(a, b, c);

		System.out.println("delta " + delta);
		
		int raiz1 = raiz1(a, b, c);
		
		System.out.println("raiz 1: " + raiz1);
		
		int raiz2 = raiz2(a, b, c);
		
		System.out.println("raiz 2: " + raiz2);
		
		System.out.println("teste raiz quadrada numeros negativos: " + qualARaizQuadradaDe(-4));
	}
	
	//OK
	static int delta(int a, int b, int c) {
		int delta = 0;
		
		b = b * 2; // multiplicar ele por ele mesmo
		
		delta = b - (4 * a * c); // formula pra encontrar o delta -> (Δ = b2 - 4.a.c)

		return delta;
	}
	
	//OK
	static int raiz1(int a, int b, int c) {
		int raiz1 = 0;
		int delta = delta(a, b, c);
		int raizQuadradaDelta = qualARaizQuadradaDe(delta);
		
		raiz1 = (-b + raizQuadradaDelta) / (2 * a); // formula para encontrar a raiz1 da equacao -> x1 = ( (-b + √Δ)/2a)
		
		return raiz1;
	}
	
	//OK
	static int raiz2(int a, int b, int c) {
		int raiz2 = 0;
		int delta = delta(a, b, c);
		int raizQuadradaDelta = qualARaizQuadradaDe(delta);
		
		raiz2 = (-b - raizQuadradaDelta) / (2 * a) ; // formula para encontrar a raiz2 da equacao -> x2    = ( (-b - √Δ)/2a)
		
		return raiz2;
	}
	
	//OK
	static int qualARaizQuadradaDe(int numero) {
		int raizQuadrada = 0;
		// calcular apenas raiz quadrada de numeros positivos comentar o else abaixo
		/*if(numero < 0) {
			System.out.println("numero negativo para calculo de raiz quadrada " + numero);
			String numeroEmTexto = String.valueOf(numero); // converter o numero negativo em texto
			numeroEmTexto = numeroEmTexto.replace("-", ""); // remover o sinal de negativo por vazio
			numero = Integer.valueOf(numeroEmTexto); // converter o numero em texto para inteiro
		}*/
		
		if(numero > 0) {
			for(int i = 0; i <= numero; i++) {
				if(i * i == numero) { // verifica se o numero da iteracao e o mesmo numero informado para o calculo
					raizQuadrada = i;
					i = numero + 1; // muda o contador para finalizar a iteracao
				}
			}
		} else {
			// calcular raiz quadrada de numeros negativos
			for(int i = 0; i >= numero; i--) {
				if(-(i * i) == numero) { // verifica se o numero da iteracao e o mesmo numero informado para o calculo
					raizQuadrada = i;
					i = numero + 1; // muda o contador para finalizar a iteracao
				}
			}
		}
		
		return raizQuadrada;
	}

}