import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DiagonalDifference {

	public static void main(String[] args) {
		List<List<Integer>> squareMatrix = new ArrayList<>();

		List<Integer> lineFirst = Arrays.asList(11,2,4);
		List<Integer> lineSecond = Arrays.asList(4,5,6);
		List<Integer> lineThird = Arrays.asList(10,8,-12);
		
		squareMatrix.add(lineFirst);
		squareMatrix.add(lineSecond);
		squareMatrix.add(lineThird);
		
		System.out.println(diagonalDifference(squareMatrix));
	}
	
	public static int diagonalDifference(List<List<Integer>> arr) {
		int rows = rows(arr);
		int columns = columns(arr);
		
		System.out.println("rows " + rows);
		System.out.println("columns " + columns);
		
		
		/*int[] leftToRightValues = {arr.get(0).get(0), 
				                   arr.get(1).get(1), 
				                   arr.get(2).get(2)};*/

		int[] leftToRightValues = new int[columns];
		// get values left to right dinamic
		for(int i = 0; i < rows; i++) {
			leftToRightValues[i] = arr.get(i).get(i); // row and colum is equals 
		}
		
		// get values right to left
		/*int[] rightToLeftValues = {arr.get(0).get(2), 
				                   arr.get(1).get(1), 
				                   arr.get(2).get(0)};*/
		
		int[] rightToLeftValues = new int[columns];
		// get values right to left dinamic
		for(int i = 0; i < rows; i++) {
			rightToLeftValues[i] = arr.get(i).get((columns -1) - i); // get colum inverter
		}
		
		// sum values left to right
		int sumLeftToRight = 0;
		for(int i=0;i<leftToRightValues.length;i++) {
			sumLeftToRight += leftToRightValues[i];
		}
		
		// sum values right to left
		int sumRightToLeft = 0;
		for(int i=0;i<rightToLeftValues.length;i++) {
			sumRightToLeft += rightToLeftValues[i];
		}
		
		// condition for remove negative numbers
		if(sumLeftToRight > sumRightToLeft)
			return (sumLeftToRight - sumRightToLeft);
		
	 return (sumRightToLeft - sumLeftToRight);
    }
	
	static int rows(List<List<Integer>> arr) {
		 return arr.size();
	}
	
	static int columns(List<List<Integer>> arr) {
		// validade if is square matrix
		validadeIfIsSquareMatrix(arr);
		
		// get first row
		List<Integer> firstRow = arr.get(0); 
		
		// first line colums number
		int colums = firstRow.size();
		
		return colums;
	}
	
	static void validadeIfIsSquareMatrix(List<List<Integer>> arr) {
		// get rows numbers of square matrix
		int rows = rows(arr);
		
		// validade if is square matrix
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < rows; j++) {
				if (arr.get(i).size() != arr.get(j).size())
					throw new RuntimeException("it's not Square Matrix");
			}
		}
	}
	
}