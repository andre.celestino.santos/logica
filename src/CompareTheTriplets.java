import java.util.Arrays;
import java.util.List;

public class CompareTheTriplets {

	public static void main(String[] args) {
		List<Integer> a = Arrays.asList(5,6,7);
		List<Integer> b = Arrays.asList(3,6,10);
		
		System.out.println(compareTriplets(a, b));
	}
	
	 // Complete the compareTriplets function below.
    static List<Integer> compareTriplets(List<Integer> a, List<Integer> b) {
    	List<Integer> points = Arrays.asList(0,0);
        int size = a.size() == b.size() ? a.size() : 0;
        if(size > 0){
            for(int i = 0; i < size; i++){
            	if(a.get(i) > b.get(i)) { //If a[i] > b[i], then Alice is awarded 1 point.
            		Integer pointsA = points.get(0); // get value the Alice
            		pointsA += 1; // increment one at variable pointsA
            		points.set(0, pointsA); // override with new value for Alice
            	} else if(a.get(i) < b.get(i)) { // If a[i] < b[i], then Bob is awarded 1 point.
            		Integer pointsB = points.get(1); // get value the Bob
            		pointsB += 1; // increment one at variable pointsA
            		points.set(1, pointsB); // override with new value for Bob
            	}
            }
        }
        return points;
    }

}
