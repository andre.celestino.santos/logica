/*
 http://devfuria.com.br/logica-de-programacao/mdc/
 Fa�a um programa para calcular o MDC (m�ximo divisor comum) entre dois n�meros.

 Aconselho a utilizar o m�todo de divis�es sucessivas, 
 pois voc� ver� que a forma como costumamos resolver problemas matem�ticos na m�o nem sempre � o melhor caminho quando estamos codificando.
 */
public class MaximoDivisorComum {

	public static void main(String[] args) {
		//int numero1 = 24; // mdc 3
		//int numero2 = 9; // mdc 3
		
		//int numero1 = 30; // mdc 10
		//int numero2 = 20; // mdc 10
		
		//int numero1 = 12; // mdc 3
		//int numero2 = 9; // mdc 3
		
		//int numero1 = 40; // mdc 10
		//int numero2 = 50; // mdc 10
		
		int numero1 = 70; // mdc 5
		int numero2 = 65; // mdc 5

		int[] primos;
		
		// verifica qual numero maior para geracao dos primos
		if(numero1 >= numero2) {
			primos = primos(numero1);
		} else {
			primos = primos(numero2);
		}
		
		int mdc = 1;
		
		for(int i = 0; i < primos.length; i++) {
			int numeroPrimo = primos[i];
			
			// verificar a divisao dos dois numeros se os dois forem divisiveis devemos guardar
			while((numeroPrimo != 0 ) && 
			      (numero1 % numeroPrimo == 0) && 
			      (numero2 % numeroPrimo == 0)) {
			
				mdc *= numeroPrimo; // multiplicar o mdc com os numeros primos
				
				numero1 = numero1 / numeroPrimo; // divisao com base no numero primo
				numero2 = numero2 / numeroPrimo; // divisao com base no numero primo
			}
		}
		
		System.out.println("MDC: " + mdc);
	}
	
	static int[] primos(int numeros) {
		int unidade = 1;
		int[] primos = new int[numeros];
		
		for(int i = 1; i <= numeros; i++) {
			int divisaoUnidade = i % unidade;
			int ePrimo = 0;
			
			if(divisaoUnidade == 0) { // verifica divisao por "1"
				ePrimo += 1; // soma 1 para as verificacoes

				// verificar se o numero � divisivel por algum outro numero se for nao � primo
				for(int j = 2; j <= i; j++) {
					if(i % j == 0) // verifica divisao por ele mesmo
						ePrimo += 1; // soma 1 para verificacoes
				}
				
				if(ePrimo == 2) // imprimir somente se for divisivel por 1 ou por ele mesmo
					adicionar(primos, i);
			}
		}
		
		return primos;
	}
	
	static void adicionar(int[] array, int valor) {
		for(int i = 0; i < array.length; i++) {
			if(array[i] == 0) {
				array[i] = valor;
				break;
			}
		}
	}
	
	static void imprimir(int[] array) {
		for(int i = 0; i < array.length; i++) {
			int valor = array[i];
			if(valor != 0)
				System.out.println(array[i]);
		}
	}

}