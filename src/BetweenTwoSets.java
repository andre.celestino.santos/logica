import java.util.Arrays;
import java.util.List;

public class BetweenTwoSets {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Integer> a = Arrays.asList(2, 6);
		List<Integer> b = Arrays.asList(24, 36);
		
//		List<Integer> a = Arrays.asList(2, 4);
//		List<Integer> b = Arrays.asList(16, 32, 96);
		
		System.out.println(getTotalX(a, b));
	}
	
	
	public static int getTotalX(List<Integer> a, List<Integer> b) {
		// Write your code here
		int numberExist = 0;
		
		for(int i = 2; i <= 32 ; i++) { // 1 fator multiplicador
			
			for(int j = 0; j < a.size(); j++) { // 2 fator multiplicador para realizar a multiplicacao para todos os elementos do array a
				int number = 0; // numero inteiro recebido da multiplicacao
				int isMultiplicateArrayA = 0;
				int isMultiplicateArrayB = 0;
				
				number = i * a.get(j); 
				
				for(int h = 0; h < a.size(); h++) { // dado a multiplicacao agora precimos verificar se todos os elementos � passivel de divisao
					
					if(a.get(h) > number) { // if para evitar a divisao errada
						if(a.get(h) % number == 0) // se a divisao nao for exata para o loop
							isMultiplicateArrayA++;
					} else {
						if(number % a.get(h) == 0) // se a divisao nao for exata para o loop
							isMultiplicateArrayA++;
					}
				}
				
				if(isMultiplicateArrayA == a.size()) { // verificacao o segundo array
					
					for(int k = 0; k < b.size(); k++) {
						if(b.get(k) > number) { // if para evitar a divisao errada
							if(b.get(k) % number == 0) // se a divisao nao for exata para o loop
								isMultiplicateArrayB++;
						} else {
							if(number % b.get(k) == 0) // se a divisao nao for exata para o loop
								isMultiplicateArrayB++;
						}
					}
				}
				
				if(isMultiplicateArrayA == a.size() && 
						isMultiplicateArrayB == b.size()) {
					//System.out.println(number);
					numberExist++;
				}
				
			}
			
		}
		
		return numberExist;
	}

}
