import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GradingStudents {

	public static void main(String[] args) {
		/*System.out.println(isMultipleOfFive(3));
		System.out.println(isMultipleOfFive(15));
		System.out.println(isMultipleOfFive(24));
		System.out.println(isMultipleOfFive(100));*/
		
		List<Integer> grades = Arrays.asList(73, 67, 38, 33);
		
		System.out.println(gradingStudents(grades)); // result -> 75, 67, 40, 33 
	}
	
	public static List<Integer> gradingStudents(List<Integer> grades) {
		// Write your code here
		List<Integer> newGrades = new ArrayList<>();
		
		int gradesArraySize = grades.size();
		
		for(int i = 0; i < gradesArraySize; i++) {
			int grade = grades.get(i);
			int multiple = grades.get(i);
			
			// rules application
			if(grade < 38) {
				newGrades.add(grade);
			} else {
				// increment for find the multiple of five
				while(!isMultipleOfFive(multiple)) {
					multiple++;
				}
				if((multiple - grade) < 3) {
					newGrades.add(multiple);
				} else {
					newGrades.add(grade);
				}
			}
			
		}

		return newGrades;
	}

	// verification if is multiple of five
	static boolean isMultipleOfFive(int number) {
		return number % 5 == 0 ? true : false;
	}

}